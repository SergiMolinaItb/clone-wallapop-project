package cat.itb.wallapopclone.Database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ProductosDao {

    @Query("SELECT * FROM Producto")
    List<Productos> getAll();

    @Insert
    void insert(Productos p);

    @Update
    void update(Productos p);

    @Delete
    void delete(Productos p);
}
