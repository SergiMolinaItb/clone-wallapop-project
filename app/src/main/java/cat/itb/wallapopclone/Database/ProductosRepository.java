package cat.itb.wallapopclone.Database;

import java.util.List;

public class ProductosRepository {
    ProductosDao dao;

    public ProductosRepository(ProductosDao dao){
        this.dao = dao;
    }

    public List<Productos> getAllProductos(){
        return dao.getAll();
    }

    public void insertProducto(Productos p){
        dao.insert(p);
    }

    public void updateProducto(Productos p){
        dao.update(p);
    }

    public void deleteProducto(Productos p){
        dao.delete(p);
    }
}
