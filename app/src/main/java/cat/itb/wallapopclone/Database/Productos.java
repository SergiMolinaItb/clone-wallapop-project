package cat.itb.wallapopclone.Database;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Producto")
public class Productos {
    @PrimaryKey
    @NonNull
    private String titulo;
    private String descripción;
    private String categoria;
    private String subcategoria;
    private String estadoProducto;
    private double precio;
    private String peso;

    public Productos(String titulo, String descripción, String categoria, String subcategoria, String estadoProducto, double precio, String peso) {
        this.titulo = titulo;
        this.descripción = descripción;
        this.categoria = categoria;
        this.subcategoria = subcategoria;
        this.estadoProducto = estadoProducto;
        this.precio = precio;
        this.peso = peso;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripción() {
        return descripción;
    }

    public void setDescripción(String descripción) {
        this.descripción = descripción;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getSubcategoria() {
        return subcategoria;
    }

    public void setSubcategoria(String subcategoria) {
        this.subcategoria = subcategoria;
    }

    public String getEstadoProducto() {
        return estadoProducto;
    }

    public void setEstadoProducto(String estadoProducto) {
        this.estadoProducto = estadoProducto;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }
}
