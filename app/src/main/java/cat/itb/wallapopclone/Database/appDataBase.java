package cat.itb.wallapopclone.Database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Productos.class}, version = 1)
public abstract class appDataBase extends RoomDatabase {

    public static appDataBase INSTANCE;

    public abstract ProductosDao productosDao();

    public static appDataBase getInstance(Context context){
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context, appDataBase.class, "Productos,db")
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return INSTANCE;
    }
}
