package cat.itb.wallapopclone.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import cat.itb.wallapopclone.Activities.MainScreenActivity;
import cat.itb.wallapopclone.R;

public class RegisterFragment extends Fragment {

    TextInputLayout Nombre;
    TextInputLayout Email;
    TextInputLayout Contraseña;
    TextInputEditText nombre;
    TextInputEditText email;
    TextInputEditText contraseña;
    Button button_login;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.register_fragment, container, false);
        Nombre = v.findViewById(R.id.nombre_text_inputLayout);
        Email = v.findViewById(R.id.email_text_inputLayout);
        Contraseña = v.findViewById(R.id.contraseña_text_inputLayout);
        nombre = v.findViewById(R.id.nombreText);
        email = v.findViewById(R.id.emailText);
        contraseña = v.findViewById(R.id.contraseñaText);
        button_login = v.findViewById(R.id.iniciarButton);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.nombreText:
                        verificarButton();
                        break;
                    case R.id.emailText:
                        String verificarEmail = email.getText().toString();
                        if(verificarEmail.isEmpty()){
                            Email.setError("                                                            Por favor, Introduce tu email");
                        }else{
                            Email.setError(null);
                        }
                        verificarButton();
                        break;
                    case R.id.contraseñaText:
                        verificarButton();
                        break;
                    case R.id.iniciarButton:
                        Intent i = new Intent(getActivity(), MainScreenActivity.class);
                        i.putExtra("Email", email.getText().toString());
                        startActivity(i);
                        break;
                }
            }
        };

        button_login.setOnClickListener(listener);

        nombre.setOnClickListener(listener);

        email.setOnClickListener(listener);

        contraseña.setOnClickListener(listener);

        return v;
    }

    public void verificarButton(){
        String verificarNombre = nombre.getText().toString();
        String verificarEmail = email.getText().toString();
        String verificarPassword = contraseña.getText().toString();
        if(!verificarNombre.isEmpty() && !verificarEmail.isEmpty() && !verificarPassword.isEmpty()){
            button_login.setClickable(true);
        }else{
            button_login.setClickable(false);
        }
    }
}
