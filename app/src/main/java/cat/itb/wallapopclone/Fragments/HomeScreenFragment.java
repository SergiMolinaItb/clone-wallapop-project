package cat.itb.wallapopclone.Fragments;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.google.android.material.button.MaterialButton;

import cat.itb.wallapopclone.Activities.MainScreenActivity;
import cat.itb.wallapopclone.R;

public class HomeScreenFragment extends Fragment {

    private VideoView videoView;
    private MaterialButton google;
    private MaterialButton facebook;
    private MaterialButton register;
    private TextView loginText;

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.home_screen_fragment, container, false);

        google = v.findViewById(R.id.googleButton);
        facebook = v.findViewById(R.id.facebookButton);
        register = v.findViewById(R.id.registerButton);
        loginText = v.findViewById(R.id.textLogin2);

        videoStart(v);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.facebookButton:
                    case R.id.googleButton:
                        Intent i = new Intent(getActivity(), MainScreenActivity.class);
                        startActivity(i);
                        break;
                    case R.id.registerButton:
                        Navigation.findNavController(v).navigate(R.id.action_homeScreenFragment_to_registerFragment);
                        break;
                    case R.id.textLogin2:
                        Navigation.findNavController(v).navigate(R.id.action_homeScreenFragment_to_loginFragment);
                        break;
                }
            }
        };

        google.setOnClickListener(listener);
        facebook.setOnClickListener(listener);
        register.setOnClickListener(listener);
        loginText.setOnClickListener(listener);

        return v;
    }

    public void videoStart(View v) {
        videoView = v.findViewById(R.id.backgroundVideo);
        Uri uri = Uri.parse("android.resource://"
                + getContext().getPackageName()
                + "/"
                + R.raw.video2);

        videoView.setVideoURI(uri);

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });

        videoView.start();
    }
}
